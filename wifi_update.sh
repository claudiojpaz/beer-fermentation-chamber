#!/bin/bash
IP_ADDRESS=192.168.0.21

mos build --arch=esp8266

if [ $? -eq 0 ]
then
  BUILD_OK=1
else
  BUILD_OK=0
  printf 'Build Fail\n'
fi

if [ $BUILD_OK -eq 1 ]
then
  printf 'Flashing...'
  curl -F commit_timeout=30 -F file=@build/fw.zip http://$IP_ADDRESS/update
fi
if [ $? -eq 0 ]
then
  FLASH_OK=1
else
  FLASH_OK=0
  printf 'Flash Fail\n'
fi

UNDO=1
TIME=30

if [ $BUILD_OK -eq 1 ] && [ $FLASH_OK -eq 1 ]
then
  for (( i=$TIME; i>0; i--));
  do
    printf '\rReverting changes in %2d seconds. Hit any key to commit changes. ' $i
    read -s -n 1 -t 1 key
    if [ $? -eq 0 ]
    then
      UNDO=0
      break
    fi
  done

  if [ $UNDO -eq 0 ]
    then
    echo "Commiting"
    curl http://$IP_ADDRESS/update/commit
  else
    echo "Reverting"
  fi
fi
