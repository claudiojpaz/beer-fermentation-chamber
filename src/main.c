#include <stdbool.h>
#include <stdio.h>

#include "mgos.h"
#include "mgos_app.h"
#include "mgos_wifi.h"
#include "mgos_gpio.h"
#include "mgos_onewire.h"
#include "mgos_sys_config.h"
#include "mgos_timers.h"

#include "dallastemp.h"

#define LED_GPIO 2
#define FUN_GPIO 12
#define ONEWIRE_GPIO 0
#define ON 0
#define OFF 1

const char* influxdb_address = "udp://192.168.0.28:4444";

float tank_A_temp = 0;
float chamber_temp = 0;
float cool_chamber_temp = 0;
float room_temp = 0;
int fun_state = 0;

#ifndef TEMP_UPDATE_INTERVAL
    #define TEMP_UPDATE_INTERVAL 5000
#endif

#ifndef TEMP_EMA_SPAN
    #define TEMP_EMA_SPAN 12
#endif

#define _EMA(c, p, s) ((p) + (2.0 / ((s) + 1)) * ((c) - (p)))
#if TEMP_EMA_SPAN >= 1
    #define TEMP_EMA(c, p, s) _EMA(c, p, s)
#else
    #define TEMP_EMA(c, p, s) (c)
#endif

#define SMOOTH_TEMP(c, p) TEMP_EMA(c, p, TEMP_EMA_SPAN)

/* Ambiente 28:ff:50:cb:70:16:4:b8 */
/* Fermentador capsula 28:ff:35:a2:94:16:5:7f */
/* Cámara 28:ff:89:42:72:16:3:93*/
/* Cámara fria */
const unsigned char room_temperature_sensor_id[8] = {0x28,0xff,0x50,0xcb,0x70,0x16,0x4,0xb8};
const unsigned char fermentor_temperature_sensor_id[8] = {0x28,0xff,0x35,0xa2,0x94,0x16,0x5,0x7f};
const unsigned char fermentor_chamber_temperature_sensor_id[8] = {0x28,0xff,0x89,0x42,0x72,0x16,0x3,0x93};
const unsigned char cool_chamber_temperature_sensor_id[8] = {0x28,0xff,0xcb,0xe0,0x71,0x16,0x4,0x30};

struct mgos_onewire *ow = NULL;
struct dallastemp *dt = NULL;

bool led_is_on;

bool equal_rom(const unsigned char *rom, const unsigned char *sensor_rom) {
  int i;
  bool equal = true;

  for ( i = 0; i < 8 ; i++ )
    if ( rom[i] != sensor_rom[i] )
      equal = false;

  return equal;
}

static void blink_cb(void* arg)
{
  (void)arg;

  if ( led_is_on )
  {
    printf("Led is Off!\n");
    mgos_gpio_write(LED_GPIO, OFF);
    led_is_on = false;
  }
  else
  {
    printf("Led is On!\n");
    mgos_gpio_write(LED_GPIO, ON);
    led_is_on = true;
  }
}

static void fun_control_cb(void* arg)
{
  (void)arg;

  if (chamber_temp < mgos_sys_config_get_temperature_down_limit())
  {
    fun_state = 0;
    mgos_gpio_write(FUN_GPIO, OFF);
  }

  if (chamber_temp > mgos_sys_config_get_temperature_up_limit())
  {
    fun_state = 1;
    mgos_gpio_write(FUN_GPIO, ON);
  }

}


static void send_data_cb(void* arg)
{
  (void)arg;

  struct mg_connection* udp = mg_connect(mgos_get_mgr(), influxdb_address, NULL, NULL);

  mg_printf(udp, "temperatura,host=fermentador ");
  mg_printf(udp, "ambiente=%f,", room_temp);
  mg_printf(udp, "interna_A=%f,", tank_A_temp);
  mg_printf(udp, "chamber=%f,", chamber_temp);
  mg_printf(udp, "cool=%f,", cool_chamber_temp);
  mg_printf(udp, "fun_state=%d,", fun_state);
  mg_printf(udp, "up_limit=%d,", mgos_sys_config_get_temperature_up_limit());
  mg_printf(udp, "down_limit=%d", mgos_sys_config_get_temperature_down_limit());

  mg_send(udp, "", 0);
  udp->flags |= MG_F_SEND_AND_CLOSE;
}


static void dallastemp_temp_cb(const unsigned char *rom, int raw_temp) {
  float temp = dallastemp_raw_to_c(raw_temp);

  LOG(LL_INFO, ("Temp of %x:%x:%x:%x:%x:%x:%x:%x %d (%04f)",
                rom[0], rom[1], rom[2], rom[3],
                rom[4], rom[5], rom[6], rom[7],
                raw_temp, temp));

  printf("Temp of %x:%x:%x:%x:%x:%x:%x:%x %d (%04f)\n",
                rom[0], rom[1], rom[2], rom[3],
                rom[4], rom[5], rom[6], rom[7],
                raw_temp, temp);

  if (equal_rom(rom, room_temperature_sensor_id))
  {
    printf("Temperatura Ambiente: %f\n", temp);
    room_temp = temp;
  }

  if (equal_rom(rom, fermentor_temperature_sensor_id))
  {
    printf("Temperatura Fermentador: %f\n", temp);
    tank_A_temp = temp;
  }

  if (equal_rom(rom, fermentor_chamber_temperature_sensor_id))
  {
    printf("Temperatura Camara: %f\n", temp);
    chamber_temp = temp;
  }

  if (equal_rom(rom, cool_chamber_temperature_sensor_id))
  {
    printf("Temperatura Camara fria: %f\n", temp);
    cool_chamber_temp = temp;
  }

}

static void temp_timer_cb(void *arg) {
  struct dallastemp_device *device;
  printf("Temp Timer CB\n");

  if (dt == NULL) {
      return;
  }

  SLIST_FOREACH(device, &dt->devices, devices) {
      dallastemp_temp(dt, device->rom, dallastemp_temp_cb);
  }

  (void) arg;
}

enum mgos_app_init_result mgos_app_init(void) {
    mgos_gpio_set_mode(LED_GPIO, MGOS_GPIO_MODE_OUTPUT);
    mgos_gpio_set_pull(LED_GPIO, MGOS_GPIO_PULL_NONE);
    mgos_gpio_write(LED_GPIO, OFF);
    mgos_gpio_set_mode(FUN_GPIO, MGOS_GPIO_MODE_OUTPUT);
    mgos_gpio_set_pull(FUN_GPIO, MGOS_GPIO_PULL_NONE);
    mgos_gpio_write(FUN_GPIO, OFF);
    led_is_on = false;

    ow = mgos_onewire_create(ONEWIRE_GPIO);
    if (ow == NULL) {
        return MGOS_APP_INIT_ERROR;
    }

    dt = dallastemp_init(ow);
    if (dt == NULL) {
        return MGOS_APP_INIT_ERROR;
    }

    dallastemp_begin(dt);

    mgos_set_timer(1000 /* ms */,
                   true /* repeat */,
                   temp_timer_cb, NULL);

    mgos_set_timer(2000 /* ms */,
                   true /* repeat */,
                   blink_cb, NULL);

    mgos_set_timer(5000 /* ms */,
                   true /* repeat */,
                   fun_control_cb, NULL);

    mgos_set_timer(5000 /* ms */,
                   true /* repeat */,
                   send_data_cb, NULL);

  return MGOS_APP_INIT_SUCCESS;
}
